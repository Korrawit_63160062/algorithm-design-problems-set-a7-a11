/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package problemsA7_11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

/**
 *
 * @author Acer
 */
class A1_10 {

    public static void longestSortedSubarray(int[] A) {
        
        int max = 1, len = 1, index = 0;

        for (int i = 1; i < A.length; i++) {

            if (A[i] > A[i - 1]) {
                len++;
            } else {

                if (max < len) {
                    max = len;

                    index = i - max;
                }
 
                len = 1;
            }
        }

        if (max < len) {
            max = len;
            index = A.length - max;
        }

        for (int i = index; i < max + index; i++) {
            System.out.print(A[i] + " ");
        }
    }
    
    public static void main(String[] args) throws IOException {
        String fileData = new String(Files.readAllBytes(Path.of("C:\\Users\\Acer\\Dropbox\\PC\\Documents\\NetBeansProjects\\AlgorithmsDesignMidterm\\Input Output\\test.txt")));
        int[] A = Stream.of(fileData.split(" ")).mapToInt(Integer::parseInt).toArray();
        longestSortedSubarray(A);
    }
}
