/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package problemsA7_11;

/**
 *
 * @author Acer
 */
public class A1_9 {

    public static int stringToInt(String str) {
        int tempInt;
        int toInt = 0;
        
        for (int i = 0; i < str.length(); i++) {
            
            char tempChar = str.charAt(i);
            tempInt = Character.getNumericValue(tempChar);
            int exponent = 1;
            for (int j = str.length()-1-i; j > 0; j--) {
                exponent = exponent * 10;             
            }
            toInt = toInt + ( tempInt * exponent );
        }

        return toInt;
    }

    public static void main(String[] args) {
        String str = "1234";
        System.out.println(stringToInt(str));
    }

}
